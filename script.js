/*
// OUTPUT
console.log("Hello, world")

// VARIÁVEIS
var nome = "Pedro"
let idade = 20

const altura = 1.90

console.log(nome)
console.log(idade)
console.log(altura)

nome = "Maria"

console.log(nome)


altura = 1.60 // aqui da erro. uma variavel constante nao pode ser mudada
console.log(altura)


console.log("Olá, " + nome) // também podemos printar assim

// todos os tipos primitivos possuem propriedades e métodos
console.log(nome.length)
console.log(nome.toLowerCase())


// OPERADORES RELACIONAIS
var numero1 = 1
var numero2 = 1
var numero3 = "1"

console.log(numero1 == numero2) // true
console.log(numero1 === numero2) // true
console.log(numero1 == numero3) // true
console.log(numero1 === numero3) //false


// INPUT
var idade = prompt("Qual é a sua idade?")
console.log(idade)
console.log(idade + 2)
idade = parseInt(idade) // transforma um tipo sting em inteiro
console.log(idade + 2)


// DESAFIO 1 
var nome1 = prompt("Usuário 1, qual é o seu nome?")
var idade1 = prompt(nome1 + ", qual é o sua idade?")
var nome2 = prompt("Usuário 2, qual é o seu nome?")
var idade2 = prompt(nome2 + ", qual é a sua idade?")

console.log(nome1 + " tem " + idade1 + " anos.")
console.log(nome2 + " tem " + idade2 + " anos.")


// DESAFIO 2
var num1 = prompt("Digite um número:")
var num2 = prompt("Digite outro número:")

num1 = parseFloat(num1)
num2 = parseFloat(num2)

console.log(num1 + " + " + num2 + " = " + (num1 + num2))
console.log(num1 + " - " + num2 + " = " + (num1 - num2))
console.log(num1 + " x " + num2 + " = " + (num1 * num2))
console.log(num1 + " / " + num2 + " = " + (num1 / num2))
console.log(num1 + " % " + num2 + " = " + (num1 % num2))
console.log(num1 + " ^ " + num2 + " = " + (num1 ** num2))
console.log(num1 + " é maior ou igual a " + num2 + " : " + (num1 >= num2))
console.log(num1 + " é menor ou igual a " + num2 + " : " + (num1 <= num2))
console.log(num1 + " é maior do que " + num2 + " : " + (num1 > num2))
console.log(num1 + " é menor do que " + num2 + " : " + (num1 < num2))


// DESAFIO 3
var num1 = prompt("Digite um número:")
var num2 = prompt("Digite outro número:")

num1 = parseFloat(num1)
num2 = parseFloat(num2)

var op = prompt("Escolha uma opção:\n0 - Adição\n1 - Subtração\n2 - Multiplicação\n3 - Divisão\n4 - Potenciação")

if(op == 0) {
    console.log(num1 + num2)
} else if(op == 1) {
    console.log(num1 - num2)
} else if(op == 2) {
    console.log(num1 * num2)
} else if(op == 3) {
    console.log(num1 / num2)
} else if(op == 4) {
    console.log(num1 ** num2)
} else {
    console.log("Digitou errado")
}


// DESAFIO 4
var num1 = prompt("Digite um número:")
for(var cont = 0; cont <= 10; cont++){
    console.log(num1 + " x " + cont + " = " + num1*cont)
}


// DESAFIO 5

var op = 0
while(op != 6){

    var num1 = prompt("Digite um número:\n(Digite 'Sair' para sair)")
    if(num1 == "Sair") break
    var num2 = prompt("Digite outro número:\n(Digite 'Sair' para sair)")
    if(num2 == "Sair") break

    num1 = parseFloat(num1)
    num2 = parseFloat(num2)

    var op = prompt("Escolha uma opção:\n0 - Adição\n1 - Subtração\n2 - Multiplicação\n3 - Divisão\n4 - Potenciação\n5 - Mudar os números\n6 - Sair")

    if(op == 0) {
        window.alert(num1 + num2)
    } else if(op == 1) {
        window.alert(num1 - num2)
    } else if(op == 2) {
        window.alert(num1 * num2)
    } else if(op == 3) {
        window.alert(num1 / num2)
    } else if(op == 4) {
        window.alert(num1 ** num2)
    } else if(op == 5) {
        continue
    } else if(op == 6) {
        break
    } else {
        window.alert("ERRO")
    }
}
*/

// DESAFIO 6

var item = []
var matriz = []
var cont = 0

do{
    var nome = prompt("Nome:")
    item.push(nome)
    var idade = prompt("Idade:")
    item.push(idade)

    matriz[cont] = item

    var op = prompt("Quer inserir mais uma idade?(S/N)")
    cont++

} while(op != "N")

console.log("Foram realizados " + matriz.length + " cadastros")
console.log(matriz)
for(var cont = 0; cont < matriz.length; cont++){
    console.log(matriz[cont][0] + " com " + matriz[cont][1] + " anos")
}